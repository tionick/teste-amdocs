import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

/* Main Class. Contains functions for add Customer, filter Customer, set different status for Customer,
remove Customer, and OnChange event to support other functions. */
class CRUDCustomers extends React.Component {
  constructor(){
    super();
    this.state = {
      customers: [],
      inputCustomer: "",
      inputStatus: "",
      filterRegistered: true,
      filterPending: true,
      filterCanceled: true
    };

  // Add customer function
  this.addCustomer = (ev) => {
    ev.preventDefault();
    if(this.state.inputCustomer === ''){
      alert("Please set a name for the customer.");
      return;
    }
    if(this.state.inputStatus === ''){
      alert("Please select the situation of the customer.");
      return;
    }
    const customers = this.state.customers.slice();
    customers.push({ customerName: this.state.inputCustomer, customerStatus: this.state.inputStatus});
    this.setState({
      customers: customers,
      inputCustomer: "",
      inputStatus: ""
    });
  };

  // Filter customer function
  this.filterCustomer = (ev) => {
    const value = ev.target.value;

    if(value === "REGISTERED"){
      if(ev.target.checked) {
        this.setState({filterRegistered: true});
      } else {
        this.setState({filterRegistered: false});
      }
    }

    if(value === "PENDING"){
      if(ev.target.checked) {
        this.setState({filterPending: true});
      } else {
        this.setState({filterPending: false});
      }
    }

    if(value === "CANCELED"){
      if(ev.target.checked) {
        this.setState({filterCanceled: true});
      } else {
        this.setState({filterCanceled: false});
      }
    }
  };

  // Set status REGISTERED function
  this.setStatusRegistered = (index) => {
    const customers = this.state.customers.slice();
    customers[index] = {customerName: customers[index].customerName, customerStatus: "REGISTERED"};
    this.setState({customers});
  };

  // Set status PENDING function
  this.setStatusPending = (index) => {
    const customers = this.state.customers.slice();
    customers[index] = {customerName: customers[index].customerName, customerStatus: "PENDING"};
    this.setState({customers});
  };

  // Set status CANCELED function
  this.setStatusCanceled = (index) => {
    const customers = this.state.customers.slice();
    customers[index] = {customerName: customers[index].customerName, customerStatus: "CANCELED"};
    this.setState({customers});
  };

  // Remove customer function
  this.removeCustomer = (index) => {
    const customers = this.state.customers.slice();
    customers.splice(index, 1);
    this.setState({customers});
  };

  // OnChange event to support other functions
  this.onChange = (ev) => {
    const state = Object.assign({}, this.state);
    const field = ev.target.name;
    state[field] = ev.target.value;
    this.setState(state);
  };
}

  render(){
    return (
      <CustomerManagement
        customers={this.state.customers}
        inputCustomer={this.state.inputCustomer}
        inputStatus={this.state.inputStatus}
        onChange={this.onChange}
        addCustomer={this.addCustomer}
        setStatusRegistered={this.setStatusRegistered}
        setStatusPending={this.setStatusPending}
        setStatusCanceled={this.setStatusCanceled}
        removeCustomer={this.removeCustomer}
        filterCustomer={this.filterCustomer}
        filterRegistered={this.state.filterRegistered}
        filterPending={this.state.filterPending}
        filterCanceled={this.state.filterCanceled}
      />
    );
  }
}


/* Class to list customers, based on the filters set on current state */
class ListCustomers extends React.Component {
  constructor(props){
    super(props);

    this.setStatusRegistered = () => {
      this.props.setStatusRegistered(this.props.index);
    };

    this.setStatusPending = () => {
      this.props.setStatusPending(this.props.index);
    };

    this.setStatusCanceled = () => {
      this.props.setStatusCanceled(this.props.index);
    };

    this.removeCustomer = () => {
      this.props.removeCustomer(this.props.index);
    };

  }

  render(){
    const statusCustomer = this.props.customerStatus;
    let tableLine;

    if(statusCustomer === "REGISTERED" && this.props.filterRegistered === false){
      tableLine = "";
    }
    else if(statusCustomer === "PENDING" && this.props.filterPending === false){
      tableLine = "";
    }
    else if(statusCustomer === "CANCELED" && this.props.filterCanceled === false){
      tableLine = "";
    }
    else {
      tableLine =
      <tr>
        <td>{this.props.customerName}</td>
        <td>
          <ControlerCustomer
            index={this.props.index}
            setStatusRegistered={this.props.setStatusRegistered}
            setStatusPending={this.props.setStatusPending}
            setStatusCanceled={this.props.setStatusCanceled}
            customerStatus={this.props.customerStatus}
          />
          <i className="fas fa-trash-alt" onClick={this.removeCustomer}></i>
        </td>
      </tr>;
    }

    return(
      tableLine
    );
  }
}


/* Create the customer panel to change status and/or remove */
class ControlerCustomer extends React.Component {
  constructor(props){
    super(props);

    this.setStatusRegistered = () => {
      this.props.setStatusRegistered(this.props.index);
    };

    this.setStatusPending = () => {
      this.props.setStatusPending(this.props.index);
    };

    this.setStatusCanceled = () => {
      this.props.setStatusCanceled(this.props.index);
    };

  }

  render() {
    const statusCustomer = this.props.customerStatus;
    let btnRegistered, btnPending, btnCanceled;

    if(statusCustomer === "REGISTERED"){
      btnRegistered = <i className="far fa-check-circle status-active"></i>
    } else {
      btnRegistered = <i className="far fa-check-circle" onClick={this.setStatusRegistered}></i>
    }

    if(statusCustomer === "PENDING"){
      btnPending = <i className="far fa-question-circle status-active"></i>
    } else {
      btnPending = <i className="far fa-question-circle" onClick={this.setStatusPending}></i>
    }

    if(statusCustomer === "CANCELED"){
      btnCanceled = <i className="far fa-times-circle status-active"></i>
    } else {
      btnCanceled = <i className="far fa-times-circle" onClick={this.setStatusCanceled}></i>
    }

    return(
      <span>{btnRegistered}
      {btnPending}
      {btnCanceled}</span>
    );
  }
}


// Render code for main
const CustomerManagement = (props) => (
  <div className="main">
    <h4>Customer Management</h4>
    <div className="container">
      <div className="row">
        <div className="col-5">
          <input type="text" className="form-control" name="inputCustomer" onChange={props.onChange} value={props.inputCustomer} placeholder="Who you want to register?" />
        </div>
        <div className="col-4">
          <select className="form-control" name="inputStatus" value={props.inputStatus} onChange={props.onChange}>
            <option value="">What's its situation?</option>
            <option value="REGISTERED">REGISTERED</option>
            <option value="PENDING">PENDING</option>
            <option value="CANCELED">CANCELED</option>
          </select>
        </div>
        <div className="col-3">
          <button type="button" className="btn btn-outline-secondary btn-block" onClick={props.addCustomer}>Save</button>
        </div>
      </div>
      <div className="row">
        <table className="table">
          <tbody>
            {props.customers.map((customer, index) => (
              <ListCustomers
                customerName={customer.customerName}
                customerStatus={customer.customerStatus}
                index={index}
                key={index}
                setStatusRegistered={props.setStatusRegistered}
                setStatusPending={props.setStatusPending}
                setStatusCanceled={props.setStatusCanceled}
                removeCustomer={props.removeCustomer}
                filterRegistered={props.filterRegistered}
                filterPending={props.filterPending}
                filterCanceled={props.filterCanceled}
              />
            ))}
          </tbody>
        </table>
      </div>
      <div className="row customer-filters">
        FILTERS:
        <input type="checkbox" name="filterCustomer" value="PENDING" onChange={props.filterCustomer} defaultChecked /> PENDING <input type="checkbox" name="filterCustomer" value="REGISTERED" onChange={props.filterCustomer} defaultChecked /> REGISTERED <input type="checkbox" name="filterCustomer" value="CANCELED" onChange={props.filterCustomer} defaultChecked /> CANCELED
      </div>
    </div>
  </div>
);

ReactDOM.render(<CRUDCustomers />, document.getElementById('root'));
registerServiceWorker();
